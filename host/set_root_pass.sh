#!/bin/sh

# Update root password and save related files to /cfg
# Copyright Brian Conway <bconway@rcesoftware.com>, see LICENSE for details

set -o errexit -o nounset -o pipefail #-o xtrace # DEBUG

if passwd root; then
  echo 'Saving root password'

  mount -s /cfg
  trap 'sync; umount /cfg; exit 1' ERR INT

  mkdir -p /cfg/{etc,var,tmp} /cfg/upgrade_overlay/{etc,home,root,var}
  chmod 700 /cfg/upgrade_overlay/root

  for saver in master.passwd spwd.db; do
      tar cf - -C /etc ${saver}|tar xvpf - -C /cfg/etc
  done

  sync
  umount /cfg
fi
