#!/bin/sh

# Upgrade the inactive root partition with a new filesystem and activate it
# Copyright Brian Conway <bconway@rcesoftware.com>, see LICENSE for details

set -o errexit -o nounset -o pipefail #-o xtrace # DEBUG

BINDIR=$(dirname ${0})
. ${BINDIR}/resflash.sub

MACHINE=$(machine)
set_attr_by_machine ${MACHINE}
tst=$(date +%s)

# Parse out disks and partitions

DUID=$(grep ' / ' /etc/fstab|cut -d ' ' -f 1|cut -d . -f 1)
CURRPART=$(grep ' / ' /etc/fstab|cut -d ' ' -f 1|cut -d . -f 2)
if [ ${CURRPART} = 'd' ]; then
  NEWPART=e
else
  NEWPART=d
fi
rootpart=$(mount|grep ' on / '|cut -d ' ' -f 1|cut -d / -f 3)
DISK=${rootpart%?}

# Leave one set of logs for debugging
rm -rf /tmp/resflash.??????
MNTPATH=$(mktemp -t -d resflash.XXXXXX)

# Write filesystem to the inactive partition

echo 'Writing filesystem to inactive partition'
st=$(date +%s)

if [ -n "${XXHALG+1}" ]; then
  # Pass -q to xxhsum to prevent it from clearing the line
  (tee /dev/fd/3|dd of=/dev/r${DISK}${NEWPART} ibs=8k obs=1m >> \
  ${MNTPATH}/00.upgrade.dd.xxhsum 2>&1) 3>&1|xxhsum -H${XXHALG} -q
else
  (tee /dev/fd/3|dd of=/dev/r${DISK}${NEWPART} ibs=8k obs=1m >> \
  ${MNTPATH}/01.upgrade.dd.cksum 2>&1) 3>&1|cksum -a ${ALG}
fi
echo "Writing filesystem complete ($(($(date +%s) - st))s)"

# Verify the newly written partition

echo 'Checking filesystem'
if ! fsck -fp ${DUID}.${NEWPART}; then
  echo 'Filesystem failure, fsck failed. Inspecting filesystem:'
  dd if=/dev/r${DISK}${NEWPART} bs=96k count=1 status=none|file -
  exit 1
fi

# Update fstab for the current duid and new partition

mkdir -p ${MNTPATH}/fs
mount -o noatime ${DUID}.${NEWPART} ${MNTPATH}/fs
mount -s /cfg
mount -s /mbr
trap "sync; umount ${MNTPATH}/fs; umount /cfg; umount /mbr; exit 1" ERR INT

if [ -d /cfg/upgrade_overlay ]; then
  echo 'Overlaying data from /cfg/upgrade_overlay to filesystem'
  rm -f /cfg/upgrade_overlay/etc/fstab
  tar cf - -C /cfg/upgrade_overlay .|tar xpf - -C ${MNTPATH}/fs
fi
umount /cfg

echo 'Updating fstab'
fsduid=$(grep ' / ' ${MNTPATH}/fs/etc/fstab|cut -d ' ' -f 1|cut -d . -f 1)
sed -i -e "s/${fsduid}/${DUID}/" \
    -e "/^${DUID}.d/s/${DUID}.d/${DUID}.${NEWPART}/" \
    ${MNTPATH}/fs/etc/fstab

# Update MBR

if [ -f ${MNTPATH}/fs/usr/mdec/mbr ]; then
  echo 'Updating MBR'
  fdisk -uy -f ${MNTPATH}/fs/usr/mdec/mbr ${DUID} >> \
  ${MNTPATH}/02.upgrade.fdisk.updatembr 2>&1
fi

# Update boot scaffold on arm64

if [ ${MACHINE} = 'arm64' ]; then
  echo 'Updating boot scaffold'
  cp -a ${MNTPATH}/fs/usr/mdec/rpi/* /${DOSMNT}/
  dd if=${MNTPATH}/fs/usr/mdec/pine64/u-boot-sunxi-with-spl.bin \
  of=/dev/r${DISK}c bs=1024 seek=8 conv=sync >> ${MNTPATH}/04.upgrade.dd.sunxi \
  2>&1
  echo ${DOSBOOTBINS} > /${DOSMNT}/${DOSBOOTDIR}/startup.nsh

  echo 'arm_64bit=1' > /${DOSMNT}/config.txt
  echo 'enable_uart=1' >> /${DOSMNT}/config.txt
  echo 'dtoverlay=disable-bt' >> /${DOSMNT}/config.txt
  echo 'kernel=u-boot.bin' >> /${DOSMNT}/config.txt
fi

# Update ${DOSBOOTBINS} bootloaders, if applicable

if disklabel ${DUID}|grep -q 'i:.*MSDOS' && grep -q /${DOSMNT} /etc/fstab \
&& [ -n "${DOSBOOTBINS}" ]; then
  mount -s /${DOSMNT}
  echo "Updating ${DOSBOOTBINS} bootloader(s)"
  for bootbin in ${DOSBOOTBINS}; do
    if [ -f ${MNTPATH}/fs/usr/mdec/${bootbin} ]; then
      cp ${MNTPATH}/fs/usr/mdec/${bootbin} /${DOSMNT}/${DOSBOOTDIR}/
    fi
  done
  sync
  umount /${DOSMNT}
fi

sync
umount ${MNTPATH}/fs

# Set the new partition active

echo 'Everything looks good, setting the new partition active'
if [ ${MACHINE} = 'amd64' ] || [ ${MACHINE} = 'i386' ]; then
  sed -i "/^set device hd0/s/hd0[a-p]/hd0${NEWPART}/" /mbr/etc/boot.conf
elif [ ${MACHINE} = 'arm64' ]; then
  sed -i "/^set device sd0/s/sd0[a-p]/sd0${NEWPART}/" /mbr/etc/boot.conf
fi

sync
umount /mbr

echo "Upgrade complete! ($(($(date +%s) - tst))s)"
